#if !defined(AFX_SESSIONLISTENSOCKET_H__C8FCA79A_FBD6_4020_A659_38688D497187__INCLUDED_)
#define AFX_SESSIONLISTENSOCKET_H__C8FCA79A_FBD6_4020_A659_38688D497187__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SessionListenSocket.h : header file
//

#include "SessionLink.h"
#include "FastDelegate.h"
using namespace fastdelegate;

/////////////////////////////////////////////////////////////////////////////
// CSessionListenSocket command target
class CSessionListenSocket;
struct session_accept_data
{
	int 					m_nErrorCode;		// 错误代码
	CSessionListenSocket*	m_pListenSocket;	// 接受数据的
	session_accept_data()
	{
		m_nErrorCode = 0;			
		m_pListenSocket = NULL;	
	}
};

typedef FastDelegate2<session_event*,session_accept_data*>		DelegateAcceptEventHandler;

class CSessionListenSocket : public CAsyncSocket
{
// Attributes
public:
// Operations
public:
	CSessionListenSocket();
	CSessionListenSocket(UINT nPort);
	virtual ~CSessionListenSocket();
// Overrides
public:
	BOOL IsListenState();
	void SetListenPort(UINT nPort);
	UINT GetListenPort();
	BOOL SessionListen();
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSessionListenSocket)
	public:
	virtual void OnAccept(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CSessionListenSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
	void InitMember();
	void InitEventType(session_event &event, DWORD dwType);

	BOOL							m_bListened;
	UINT							m_uListenPort;					// 监听断开
	
public:	
	DelegateAcceptEventHandler		m_AcceptEventHandler;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SESSIONLISTENSOCKET_H__C8FCA79A_FBD6_4020_A659_38688D497187__INCLUDED_)
