// SessionLink.cpp: implementation of the CSessionLink class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SessionLink.h"
#include "SessionSocket.h"
#include <algorithm>
#include <vector>
using namespace std;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
#define	 MAX_RECV_BUFF  4096
#define  MAX_SEND_BUFF  4096
CSessionLink::CSessionLink()
{
	InitMember();
}

CSessionLink::~CSessionLink()
{
	if(m_pDataSocket)
	{
		EndSession(SESSION_CLOSE_CLIENT,FALSE);
	}
	ClearMember();
}

void CSessionLink::InitMember()
{
	m_dwSessionID = AllocSessionID();
	m_dwSessionOption = 0;
	m_strServerIP.Empty();
	m_nPort = 0;
	m_pDataSocket = NULL;
	m_lsSendBufQueue.EmptyAllBufferObj();

	m_ConnectSuccEventHandler = NULL;
	m_ConnectFailEventHandler = NULL;
	m_DisconnectEventHandler = NULL;
	m_SendingDataEventHandler = NULL;
	m_RecvingDataEventHandler = NULL;
}

void CSessionLink::ClearMember()
{
	
}

DWORD CSessionLink::GetSessionID()
{
	return m_dwSessionID;
}

/*!===============================================================
  函数功能 : 为每个Session分配唯一的ID

  创建信息 : 2007年11月10日  By ZhangYuBo HEXIN Software Co., Ltd. 
	
  历史记录 : 
================================================================*/
DWORD CSessionLink::AllocSessionID()
{
	static DWORD s_dwSessionID = 0;
	s_dwSessionID ++;
	return s_dwSessionID;
}
//////////////////////////////////////////////////////////////////////////
BOOL CSessionLink::OnAccept(CAsyncSocket *pListenSocket)
{
	BOOL bRet = FALSE;
	if(pListenSocket)
	{
		m_pDataSocket = CreateSocket(TRUE);
		ASSERT(m_pDataSocket);
		if(m_pDataSocket &&
			pListenSocket->Accept(*m_pDataSocket))
		{
			SetSessionLinkType(SESSION_LINK_TYPE_LISTEN);
			m_pDataSocket->SetSocketLinkState(SOCKET_LINK_STATE_AVAILABLE);
			m_pDataSocket->AsyncSelect();
			m_pDataSocket->GetPeerName(m_strServerIP,m_nPort);
			SetSessionState(SESSION_LINK_STATE_PENDING);
			OnConnected(SESSION_CONNECT_SUCCESS);
			bRet = TRUE;
		}
	}
	return bRet;
}

void CSessionLink::OnSessionEnd(int nErrorCode)
{
	if(nErrorCode != WSAECONNABORTED)
	{
		NotifySessionFail(nErrorCode,SESSION_ERROR_SOCKET_ERROR);
	}
	if(nErrorCode == 0 || nErrorCode == WSAECONNABORTED)
	{
		EndSession(SESSION_CLOSE_REMOTE);
	}
	else
	{
		EndSession(SESSION_CLOSE_SOCKET_ERROR);
	}
}

void CSessionLink::OnConnected(int nErrorCode)
{
	::SetOptions(m_dwSessionOption,0,SESSION_ACTION_CONNECTING);
	ASSERT(IsSessionState(SESSION_LINK_STATE_PENDING));
	if(nErrorCode == 0)
	{
		SetSessionState(SESSION_LINK_STATE_AVAILABLE);
		NotifyConnectSucc();
	}	
	else if(nErrorCode)
	{
		NotifySessionFail(nErrorCode,SESSION_ERROR_CONNECT_REJECT);
		EndSession(SESSION_CLOSE_REMOTE);
	}
}

void CSessionLink::OnReceiveData()
{
	if(m_pDataSocket == NULL || 
		!IsConnected())
	{
		return;
	}
	if(GetOptions(m_dwSessionOption,SESSION_ACTION_RECEING))
	{
		return;
	}
	SetOptions(m_dwSessionOption,SESSION_ACTION_RECEING,SESSION_ACTION_RECEING);
	
	BYTE szBuffer[MAX_RECV_BUFF];
	memset(szBuffer,0,sizeof(BYTE)*MAX_RECV_BUFF);
	int nReceived = 0;
	while(m_pDataSocket != NULL && 
		(nReceived = m_pDataSocket->Receive(szBuffer,MAX_RECV_BUFF)) > 0) 
	{
		NotifyRecvData(szBuffer,nReceived);
		memset(szBuffer,0,sizeof(char)*MAX_RECV_BUFF);
	}

	if(nReceived == SOCKET_ERROR)
	{
		DWORD dwErrorCode = GetLastError();
		if(dwErrorCode == WSAEWOULDBLOCK)
		{
			if(m_pDataSocket)
			{
				m_pDataSocket->AsyncSelect();
			}
		}
	}
	SetOptions(m_dwSessionOption,0,SESSION_ACTION_RECEING);
}

void CSessionLink::OnSendData()
{
	if(m_pDataSocket == NULL || 
		IsConnected() == FALSE || 
		GetOptions(m_dwSessionOption,SESSION_ACTION_CONNECTING))
	{
		return;
	}
	
	// 已经在发送了
	if(GetOptions(m_dwSessionOption,SESSION_ACTION_SENDING))
	{
		return;
	}
	SetOptions(m_dwSessionOption,SESSION_ACTION_SENDING,SESSION_ACTION_SENDING);
	while(!m_lsSendBufQueue.IsEmpty()) 
	{
		CSendDataBuffer *pDataSend = (CSendDataBuffer*)m_lsSendBufQueue.GetBufferObj();
		if(pDataSend)
		{
			// 记录前一次通知后，发送的字节数
			ULONG lSendDataCount = 0;
			BOOL bNotify = (pDataSend->m_dwPacketFlag != 0);
			if(bNotify)
			{
				// m_ulIndexOffset 为零，认为是一个数据包开始传输的标志
				if(pDataSend->m_ulIndexOffset == 0)
				{
					NotifySendData(SEND_STATE_START,pDataSend);
				}
			}
			int nBufferSize = pDataSend->m_ulBufferSize - pDataSend->m_ulIndexOffset;
			while(nBufferSize > 0) 
			{
				int nUintSize = min(nBufferSize,MAX_SEND_BUFF);
				int nSendByte = m_pDataSocket->Send(&pDataSend->m_pBufferData[pDataSend->m_ulIndexOffset],nUintSize);
				if(nSendByte == SOCKET_ERROR)
				{
					int nError = GetLastError();
					if (nError != WSAEWOULDBLOCK) 
					{
						NotifySendData(SEND_STATE_ERROR,pDataSend);
						OnSessionEnd(nError);
					}
					else
					{
						if(bNotify)
						{
							lSendDataCount = 0;
							NotifySendData(SEND_STATE_MIDDLE,pDataSend);
						}
						if(m_pDataSocket)
						{
							m_pDataSocket->AsyncSelect();
						}
					}
					SetOptions(m_dwSessionOption,0,SESSION_ACTION_SENDING);
					return;
				}
				else
				{
					nBufferSize -= nSendByte;
					pDataSend->m_ulIndexOffset += nSendByte;
					if(bNotify)
					{
						lSendDataCount += nSendByte;
						if(lSendDataCount > pDataSend->m_nMiniNotifySize)
						{
							lSendDataCount = 0;
							NotifySendData(SEND_STATE_MIDDLE,pDataSend);							
						}
					}
				}				
			}
			// 缓冲发送完成，清除掉
			if(bNotify)
			{
				NotifySendData(SEND_STATE_MIDDLE,pDataSend);
				NotifySendData(SEND_STATE_END,pDataSend);
			}
			m_lsSendBufQueue.RemoveBufferObj();
		}
	}
	SetOptions(m_dwSessionOption,0,SESSION_ACTION_SENDING);
}

//////////////////////////////////////////////////////////////////////////

/*!===============================================================
函数功能 : 连接服务器调用

创建信息 : 2007年11月8日  By ZhangYuBo HEXIN Software Co., Ltd. 

历史记录 : 
================================================================*/
int CSessionLink::Connect(LPCSTR lpHostAddrIP, UINT nPort)
{
	if(CHECK_LPCSTR(lpHostAddrIP) == FALSE || nPort < 1)
	{
		return SESSION_CONNECT_ERR_NOSERVER;
	}
	// 如果已经连接，就先断开
	if(m_pDataSocket)
	{
		EndSession(SESSION_CLOSE_CLIENT);
	}

	ASSERT(m_pDataSocket == NULL);
	m_pDataSocket = CreateSocket(TRUE);
	ResetSession(TRUE);

	int nErrorCode = 0;
	BOOL bRet = m_pDataSocket->Create();
	if(bRet)
	{
		m_strServerIP	= lpHostAddrIP;
		m_nPort			= nPort;

		::SetOptions(m_dwSessionOption,SESSION_ACTION_CONNECTING,SESSION_ACTION_CONNECTING);
		m_pDataSocket->AsyncSelect();
		bRet = m_pDataSocket->Connect(m_strServerIP,m_nPort);
		nErrorCode = m_pDataSocket->GetLastError();

		// WSAEWOULDBLOCK 表示系统忙，操作Pending
		if(bRet || nErrorCode == WSAEWOULDBLOCK)
		{
			SetSessionLinkType(SESSION_LINK_TYPE_CONNECT);
			SetSessionState(SESSION_LINK_STATE_PENDING);
			// 操作立即完成
			if(bRet)
			{
				OnConnected(SESSION_CONNECT_SUCCESS);
			}
			return 0;
		}
	}
	nErrorCode = m_pDataSocket->GetLastError();
	SAFE_DELETE(m_pDataSocket);
	NotifySessionFail(nErrorCode,SESSION_ERROR_SOCKET_ERROR);
	return nErrorCode;
}

void CSessionLink::Disconnect(int nMode,BOOL bNotify)
{
	if(IsConnected())
	{
		EndSession(nMode,bNotify);
	}
}
/*!===============================================================
  函数功能 : 发送数据

  创建信息 : 2007年11月9日  By ZhangYuBo HEXIN Software Co., Ltd. 
	
  历史记录 : 
================================================================*/
BOOL CSessionLink::Send(BYTE *lpszBuffer,ULONG cbBuffer,DWORD dwPacketFlag,int nFlag)
{
	// 当Session没有物理连接
	if(!IsConnected())
	{
		return FALSE;
	}

	if(lpszBuffer)
	{
		CSendDataBuffer* pBufferSend = NULL;
		int nAllocFlag = BUF_NEW_FLAG;
		if((nFlag & SESSION_SEND_FLAG_MERGE) && 
			m_lsSendBufQueue.GetCount() > 0)
		{
			pBufferSend = (CSendDataBuffer*)m_lsSendBufQueue.GetLastBufferObj();
			nAllocFlag = BUF_APPEND_FLAG;
		}
		else
		{
			pBufferSend = new CSendDataBuffer();
			m_lsSendBufQueue.AddBufferObj(pBufferSend);
		}
		
		if(pBufferSend == NULL)
		{
			return FALSE;
		}
		
		// 如果有值的话，说明这个数据包的发送过程需要通知上层
		pBufferSend->m_dwPacketFlag = dwPacketFlag;
		
		// 将要发送的数据复制到待发送队列
		BYTE* pBuffer = pBufferSend->Alloc(cbBuffer, nAllocFlag);
		ASSERT(pBuffer);
		if (pBuffer != NULL)
		{
			memcpy(pBuffer, lpszBuffer, cbBuffer);
		}

	}
	
	if (!GetOptions(m_dwSessionOption,SESSION_ACTION_CONNECTING) && 
		!GetOptions(m_dwSessionOption,SESSION_ACTION_SENDING) && 
		!(nFlag & SESSION_SEND_FLAG_DELPAY))
	{
		OnSendData();
	}	
	return TRUE;	
}

BOOL CSessionLink::AddSendBuffer(CSendDataBuffer* pSendBuffer,int nFlag)
{
	m_lsSendBufQueue.AddBufferObj(pSendBuffer);	
	if (!GetOptions(m_dwSessionOption,SESSION_ACTION_CONNECTING) && 
		!GetOptions(m_dwSessionOption,SESSION_ACTION_SENDING) && 
		!(nFlag & SESSION_SEND_FLAG_DELPAY))
	{
		OnSendData();
	}	
	return TRUE;	
}

void CSessionLink::EndSession(int nMode,BOOL bNotify)
{
	if(m_pDataSocket)
	{
		if(nMode == SESSION_CLOSE_REMOTE || 
			nMode == SESSION_CLOSE_CLIENT ||
			nMode == SESSION_CLOSE_SOCKET_ERROR)
		{
			m_pDataSocket->CloseSocket();
			m_pDataSocket = NULL;
		}
		else if(nMode == SESSION_CLOSE_CONNECT_TIMEOUT)
		{
			m_pDataSocket->ClearSession();
			m_pDataSocket = NULL;
		}
	}
	if(bNotify)
	{
		NotifyDisconnet(nMode);
	}
	ResetSession(nMode != SESSION_CLOSE_REMOTE);
}

void CSessionLink::ResetSession(BOOL bEmptySendBuff)
{
	if(bEmptySendBuff)
	{
		m_lsSendBufQueue.EmptyAllBufferObj();
	}
	SetSessionState(SESSION_LINK_STATE_INVALID);
	SetOptions(m_dwSessionOption,0,SESSION_ACTION_MASK);
	m_strServerIP.Empty();
	m_nPort = 0;
}
//////////////////////////////////////////////////////////////////////////
CSessionSocket* CSessionLink::CreateSocket(BOOL bAutoDelete)
{
	CSessionSocket* pNewSocket = NULL;
	pNewSocket = new CSessionSocket(this);
	pNewSocket->EnableAutoDelete(bAutoDelete);
	return pNewSocket;
}

void CSessionLink::InitEventType(session_event &event, DWORD dwType)
{
	event.m_dwSessionID = GetSessionID();
	event.m_dwEventType = dwType;
}

/*!===============================================================
  函数功能 : Session连接失败时的通知

  创建信息 : 2007年11月9日  By ZhangYuBo HEXIN Software Co., Ltd. 
	
  历史记录 : 
================================================================*/
void CSessionLink::NotifySessionFail( int nErrorCode,DWORD dwErrorType,LPCSTR lpMessage)
{
	session_event event;
	InitEventType(event,SESSION_EVENT_CONNECT_FAIL);
	
	session_connect_fail _fail;
	_fail.m_nErrorCode = dwErrorType;
	if(CHECK_LPCSTR(lpMessage))
	{
		_fail.m_strErrorMessage = lpMessage;
	}
	else
	{
		_fail.m_strErrorMessage.Format("__SessionErrorCode__=%d",nErrorCode);
	}

	if(m_ConnectFailEventHandler)
	{
		m_ConnectFailEventHandler(&event,&_fail);
	}
}

/*!===============================================================
  函数功能 : 断开消息的通知

  创建信息 : 2007年11月9日  By ZhangYuBo HEXIN Software Co., Ltd. 
	
  历史记录 : 
================================================================*/
void CSessionLink::NotifyDisconnet(DWORD dwDisconnectMode)
{
	session_event event;
	InitEventType(event,SESSION_EVENT_DISCONNECT);
	
	session_disconnect dis;
	dis.m_dwDisconnectMode = dwDisconnectMode;
	
	if(m_DisconnectEventHandler)
	{
		m_DisconnectEventHandler(&event,&dis);
	}
}

/*!===============================================================
  函数功能 : 连接成功

  创建信息 : 2007年11月9日  By ZhangYuBo HEXIN Software Co., Ltd. 
	
  历史记录 : 
================================================================*/
void CSessionLink::NotifyConnectSucc()
{
	session_event event;
	InitEventType(event,SESSION_EVENT_CONNECT_SUCC);
	if(m_ConnectSuccEventHandler)
	{
		m_ConnectSuccEventHandler(&event);
	}
}

/*!===============================================================
  函数功能 : 发送数据时候的通知

  创建信息 : 2007年11月10日  By ZhangYuBo HEXIN Software Co., Ltd. 
	
  历史记录 : 
================================================================*/
void CSessionLink::NotifySendData(DWORD dwSendState,CSendDataBuffer* pSendBufferObj)
{
	session_event event;
	InitEventType(event,SESSION_EVENT_SENDING);
	session_send_data _send;
	_send.m_dwSendState = dwSendState;
	_send.m_pSendBufferObj = pSendBufferObj;

	if(m_SendingDataEventHandler)
	{
		m_SendingDataEventHandler(&event,&_send);	
	}
}

/*!===============================================================
  函数功能 : 接受数据的时候收到消息

  创建信息 : 2007年11月10日  By ZhangYuBo HEXIN Software Co., Ltd. 
	
  历史记录 : 
================================================================*/
void CSessionLink::NotifyRecvData(BYTE* m_pszBuffer,ULONG lRecvByteSize)
{
	if(m_pszBuffer && lRecvByteSize > 0)
	{
		session_event event;
		InitEventType(event,SESSION_EVENT_RECEVIED);
		
		session_received_data _recv;
		_recv.m_pszBuffer = m_pszBuffer;
		_recv.m_lTransLength = lRecvByteSize;
		
		if(m_RecvingDataEventHandler)
		{
			m_RecvingDataEventHandler(&event,&_recv);
		}
	}
}

//////////////////////////////////////////////////////////////////////////
DWORD CSessionLink::GetSessionLinkType()
{
	return (::GetOptions(m_dwSessionOption,SESSION_LINK_TYPE_MASK));
}

void CSessionLink::SetSessionLinkType(DWORD dwLinkType)
{
	::SetOptions(m_dwSessionOption,dwLinkType,SESSION_LINK_TYPE_MASK);
}

void CSessionLink::SetSessionState(DWORD dwState)
{
	::SetOptions(m_dwSessionOption,dwState,SESSION_LINK_STATE_MASK);
}

DWORD CSessionLink::GetSessionState()
{
	return (::GetOptions(m_dwSessionOption,SESSION_LINK_STATE_MASK));
}

BOOL CSessionLink::IsSessionState(DWORD dwSessionState)
{
	return (GetSessionState() == dwSessionState);
}

BOOL CSessionLink::IsConnected()
{
	return IsSessionState(SESSION_LINK_STATE_AVAILABLE);
}

void CSessionLink::GetSessionHostInfo(CString &strHostIP, UINT &nPort)
{
	strHostIP = m_strServerIP;
	nPort = m_nPort;
}
