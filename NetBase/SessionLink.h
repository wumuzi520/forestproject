// SessionLink.h: interface for the CSessionLink class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SESSIONLINK_H__20EE2FA7_5FB4_4954_94C2_3E3B43262786__INCLUDED_)
#define AFX_SESSIONLINK_H__20EE2FA7_5FB4_4954_94C2_3E3B43262786__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "DataBufferListQueue.h"
#include "FastDelegate.h"
#include <afxsock.h>
using namespace fastdelegate;

struct CSendDataBuffer : public CDataBufferObj
{
	CSendDataBuffer()
		: CDataBufferObj()
	{
		m_nMiniNotifySize = -1;
		m_dwPacketFlag = 0;
		m_bNoDeleteMode = FALSE;
	}

	CSendDataBuffer(CSendDataBuffer& src)
	{
		*this = src;
	}

	virtual ~CSendDataBuffer()
	{
		if(m_bNoDeleteMode)
		{
			m_pBufferData = NULL;
		}
	}

	const CSendDataBuffer& operator = (const CSendDataBuffer& src)
	{
		// 不删除内存模式，只要进行"浅复制"就可以了
		if(m_bNoDeleteMode)
		{
			m_pBufferData	= src.m_pBufferData;
			m_ulBufferSize  = src.m_ulBufferSize;		
			m_ulIndexOffset = src.m_ulIndexOffset;	
		}
		else
		{
			CDataBufferObj* pThis = this;
			const CDataBufferObj* pSrc  = &src;
			*pThis = *pSrc;
		}
		m_nMiniNotifySize = src.m_nMiniNotifySize;
		m_bNoDeleteMode = src.m_bNoDeleteMode;
		m_dwPacketFlag = src.m_dwPacketFlag;
		return (*this);
	}
	USHORT	m_nMiniNotifySize;	// 最小通知大小，当数据发送多少后，通知一次
	USHORT	m_bNoDeleteMode;	// 不删除内存模式，通常用在发送很大的文件上内存由外部分配
	DWORD	m_dwPacketFlag;		// 发送数据缓冲，用户指定意的数据，为了能在发送的时候通知调用者
								// 数据发送的情况如何
};

// Session Event 通知事件的定义
#define	SESSION_EVENT_MASK				0x0000000F		// 连接通知事件
#define	SESSION_EVENT_NONE				0x00000000		// 空
#define	SESSION_EVENT_CONNECT_SUCC		0x00000001		// 连接成功
#define	SESSION_EVENT_CONNECT_FAIL		0x00000002		// 连接失败
#define	SESSION_EVENT_DISCONNECT		0x00000003		// 断开连接
#define	SESSION_EVENT_RECEVIED			0x00000004		// 收到数据
#define	SESSION_EVENT_SENDING			0x00000005		// 发送数据
#define	SESSION_EVENT_ACCEPT			0x00000006		// Accept事件

struct session_event 
{
	DWORD	m_dwEventType;
	DWORD	m_dwSessionID;
	session_event()
	{
		m_dwEventType = SESSION_EVENT_NONE;
		m_dwSessionID = 0;
	}
	DWORD Event()
	{
		return (m_dwEventType & SESSION_EVENT_MASK);
	}
};

#define SESSION_ERROR_CODE_MASK				0x000000FF
#define SESSION_ERROR_NONE					0x00000000
#define SESSION_ERROR_SOCKET_ERROR			0x00000001		// 创建Socket失败
#define SESSION_ERROR_CONNECT_REJECT		0x00000002		// 地址连接不上
struct session_connect_fail
{
	UINT		m_nErrorCode;
	CString		m_strErrorMessage;
	session_connect_fail()
	{
		m_nErrorCode = 0;
		m_strErrorMessage = "";
	}
	UINT GetErrorCode()
	{
		return (m_nErrorCode & SESSION_ERROR_CODE_MASK);
	}
};

struct session_disconnect
{
	session_disconnect()
	{
		m_dwDisconnectMode = 0;
	}
	DWORD	m_dwDisconnectMode;
};

#define		SEND_STATE_MASK		0x0000000F
#define		SEND_STATE_INIT		0x00000000		// 未开始发送
#define		SEND_STATE_START	0x00000001		// 发送开始
#define		SEND_STATE_MIDDLE	0x00000002		// 发送过程中
#define		SEND_STATE_END		0x00000003		// 发送结束
#define		SEND_STATE_ERROR	0x00000004		// 发送错误
struct session_send_data
{
	DWORD			 m_dwSendState;			// 发送数据的状态
	CSendDataBuffer* m_pSendBufferObj;		// 发送数据的对象
	session_send_data()
	{
		m_dwSendState = SEND_STATE_INIT;		// 接受数据的状态
		m_pSendBufferObj = NULL;
	}
	DWORD GetState()
	{
		return (m_dwSendState & SEND_STATE_MASK);
	}
};

struct session_received_data
{
	ULONG		m_lTransLength;		// 已经传输的长度
	BYTE*		m_pszBuffer;		// 接受数据的
	session_received_data()
	{
		m_lTransLength = 0;						// 已经传输的长度
		m_pszBuffer = NULL;						// 接受数据的		
	}
};

//////////////////////////////////////////////////////////////////////////

#define		SESSION_SEND_FLAG_NORMAL			0			//正常处理
#define		SESSION_SEND_FLAG_MERGE				1			//合并数据包
#define		SESSION_SEND_FLAG_DELPAY			2			//延迟发送

// 连接时发生的错误
#define		SESSION_CONNECT_SUCCESS				0			//没有错误
#define		SESSION_CONNECT_ERR_NOSERVER		-1			//没有主站地址

// Session 关闭时候的选项
#define		SESSION_CLOSE_REMOTE				0x00000001	// 远程发起的断开
#define		SESSION_CLOSE_CLIENT				0x00000002	// 本地发起的断开
#define		SESSION_CLOSE_CONNECT_TIMEOUT		0x00000003	// 本地连接超时断开
#define		SESSION_CLOSE_SOCKET_ERROR			0x00000004	// Socket错误导致的关闭

// Session 的链接状态 m_dwSessionOption
#define		SESSION_LINK_STATE_MASK				0x0000000F	
#define		SESSION_LINK_STATE_INVALID			0x00000000	// 初始状态
#define		SESSION_LINK_STATE_PENDING			0x00000001	// 链接真正进行中
#define		SESSION_LINK_STATE_AVAILABLE		0x00000002	// 已经建立了有效的连接

// Session 执行的动作状态 m_dwSessionOption
#define		SESSION_ACTION_MASK					0x000000F0	
#define		SESSION_ACTION_CONNECTING			0x00000010	// 正在连接服务器
#define		SESSION_ACTION_RECEING				0x00000020	// 正在接收数据
#define		SESSION_ACTION_SENDING				0x00000040	// 正在发送数据

// Session 的链接类型 m_dwSessionOption
#define		SESSION_LINK_TYPE_MASK				0x00000F00	
#define		SESSION_LINK_TYPE_CONNECT			0x00000000	// 主动Connect类型的Session
#define		SESSION_LINK_TYPE_LISTEN			0x00000100	// 被动Listen类型的Session

/*!===============================================================
  描    述 : 封装了异步Socket发送数据和链接等方面的细节处理，对调用
			 呈现出一个方便易用的面向连接的对象

  类    名 : CSessionLink 

  基    类 : CObject 

  创建信息 : 2007年11月9日  By ZhangYuBo HEXIN Software Co., Ltd. 
	 
  历史记录 : 
================================================================*/
typedef FastDelegate1<session_event*>							DelegateConnectSuccEventHandler;
typedef FastDelegate2<session_event*,session_connect_fail*>		DelegateConnectFailEventHandler;
typedef FastDelegate2<session_event*,session_disconnect*>		DelegateDisconnectEventHandler;
typedef FastDelegate2<session_event*,session_send_data*>		DelegateSendingDataEventHandler;
typedef FastDelegate2<session_event*,session_received_data*>	DelegateRecvingDataEventHandler;

class CSessionSocket;
class CSessionLink : public CObject  
{
public:
	void GetSessionHostInfo(CString &strHostIP,UINT &nPort);
	BOOL OnAccept(CAsyncSocket* pListenSocket);
	DWORD GetSessionLinkType();
	CSessionLink();
	virtual ~CSessionLink();
	void Disconnect(int nMode = SESSION_CLOSE_CLIENT,BOOL bNotify = FALSE);
	int Connect(LPCSTR lpHostAddrIP,UINT nPort);
	BOOL IsConnected();
	BOOL Send(BYTE* lpszBuffer, ULONG cbBuffer,DWORD dwPacketFlag = 0,int nFlag = SESSION_SEND_FLAG_NORMAL);
	BOOL AddSendBuffer(CSendDataBuffer* pSendBuffer,int nFlag = SESSION_SEND_FLAG_NORMAL);
	DWORD GetSessionID();
protected:
	static DWORD AllocSessionID();
	void OnSendData();
	void OnReceiveData();
	void OnConnected(int nErrorCode);
	void OnSessionEnd(int nErrorCode);
	void ClearMember();
	void InitMember();
	void SetSessionLinkType(DWORD dwLinkType);
	BOOL IsSessionState(DWORD dwSessionState);
	void SetSessionState(DWORD dwState);
	DWORD GetSessionState();
	void ResetSession(BOOL bEmptySendBuff);
	void EndSession(int nMode,BOOL bNotify = TRUE);
	void InitEventType(session_event &event,DWORD dwType);
	void NotifySessionFail( int nErrorCode,DWORD dwErrorType,LPCSTR lpMessage = NULL);
	void NotifyDisconnet( DWORD dwDisconnectMode );
	void NotifyConnectSucc();
	void NotifySendData(DWORD dwSendState,CSendDataBuffer* pSendBufferObj);
	void NotifyRecvData(BYTE* m_pszBuffer,ULONG lRecvByteSize);
protected:
	CSessionSocket* CreateSocket(BOOL bAutoDelete);
	DWORD				m_dwSessionID;						// SessionID 每个Session在创建的时候自动产生
	DWORD				m_dwSessionOption;					// 保存Session的一些属性
	CString				m_strServerIP;						// 服务器的IP
	UINT				m_nPort;							// 服务器的端口
//	CSessionSocket*		m_pDataSocket;						// 异步Socket对象
	CDataBufferListQueue	m_lsSendBufQueue;				// 数据发送缓冲块队列
public:
	DelegateConnectSuccEventHandler			m_ConnectSuccEventHandler;
	DelegateConnectFailEventHandler			m_ConnectFailEventHandler;
	DelegateDisconnectEventHandler			m_DisconnectEventHandler;
	DelegateSendingDataEventHandler			m_SendingDataEventHandler;
	DelegateRecvingDataEventHandler			m_RecvingDataEventHandler;

	CSessionSocket*		m_pDataSocket;						// 异步Socket对象
   
	friend class CSessionSocket;
};

#endif // !defined(AFX_SESSIONLINK_H__20EE2FA7_5FB4_4954_94C2_3E3B43262786__INCLUDED_)
