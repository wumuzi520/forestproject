#if !defined(AFX_SESSIONSOCKET_H__71720D97_F553_43D6_AFCD_DC3A2738B16B__INCLUDED_)
#define AFX_SESSIONSOCKET_H__71720D97_F553_43D6_AFCD_DC3A2738B16B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SessionSocket.h : header file
//
#include <afxsock.h>

// Socket链接状态
#define		SOCKET_LINK_STATE_MASK				0x000F0000	
#define		SOCKET_LINK_STATE_INIT				0x00000000	// 初始状态
#define		SOCKET_LINK_STATE_PENDING			0x00010000	// 链接真正进行中
#define		SOCKET_LINK_STATE_AVAILABLE			0x00020000	// 已经建立了有效的连接
#define		SOCKET_LINK_STATE_DEATH				0x00030000  // 链接已经断开

#define		SOCKET_OPTION_AUTO_DELETE			0x10000000	// 具有自动删除功能的Socket

#define		SOCKET_TIME_COUNT_MASK				0x0000FFFF	// 放计时器的位置
/////////////////////////////////////////////////////////////////////////////
// CSessionSocket command target
class CSessionLink;
class CSessionSocket : public CAsyncSocket
{
// Attributes
public:

// Operations
public:
	CSessionSocket();
	CSessionSocket(CSessionLink* pSessionLink);
	virtual ~CSessionSocket();

// Overrides
public:
	void EnableAutoDelete(BOOL bEnable);
	BOOL IsAutoDelete();
	void RestTimeCount();
	DWORD GetTimeCount();
	void IncreaseTimeCount();
	
	DWORD GetSocketLinkState();
	void SetSocketLinkState(DWORD dwLinkState);
	void ClearSession();
	void CloseSocket();
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSessionSocket)
	public:
	virtual void OnClose(int nErrorCode);
	virtual void OnConnect(int nErrorCode);
	virtual void OnReceive(int nErrorCode);
	virtual void OnSend(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CSessionSocket)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
	void InitMember();
	CSessionLink*		m_pSessionLink;
	DWORD				m_dwSocketState;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SESSIONSOCKET_H__71720D97_F553_43D6_AFCD_DC3A2738B16B__INCLUDED_)
