#include <iostream>
using namespace std;

#include "tinyxml.h"

int main()
{
// 	TiXmlDocument doc;
// 	doc.LoadFile("Configuration.xml");
// 	TiXmlHandle hXml(&doc);
// 	TiXmlElement xmlElement("ContourColor");
// 	TiXmlElement* pElement = hXml.FirstChildElement("DlgChain").ToElement();
// 	pElement->InsertEndChild(xmlElement);
// 	//doc.Print(stdout);
// 	//hXml.ToNode()->Print(stdout,0);
// 	hXml.Print(stdout,0);

	CXmlParser xmlParser("..\\bin\\Configuration\\Configuration.xml");
	//xmlParser.LoadFile("Configuration.xml");
	TiXmlElement element("ContourColor");
	TiXmlElement* pElement = xmlParser.FirstChild("DlgChain").ToElement();
	pElement->InsertEndChild(element);
	pElement = pElement->FirstChild("ContourColor")->ToElement();
	pElement->SetAttribute("edge",0xffffff);
	pElement->SetAttribute("mask",0xff00ff);
	pElement->SetAttribute("num",0x0000ff);
	xmlParser.SaveAs("Configuration1.xml");
	
	system("pause");
	return 0;
}