#ifndef FOREST_PLATFORM_H_
#define FOREST_PLATFORM_H_

#if defined(linux) || defined(__linux) || defined(__linux__)
#define FOREST_PLATFORM_LINUX
#elif defined(_WIN32) || defined(__WIN32__) || defined(WIN32)
#define FOREST_PLATFORM_WINDOWS
#endif


#endif