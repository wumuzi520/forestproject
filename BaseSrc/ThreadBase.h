#ifndef FOREST_THREAD_H_
#define FOREST_THREAD_H_

#include <Windows.h>

class CThreadBase
{
public:
	CThreadBase();
	virtual ~CThreadBase();
	virtual void Run() = 0;

public:
	void Start();
	void Pause();
	void Close();
	void Wait(unsigned long ms = INFINITE);

	HANDLE GetHandle() const;


private:
	HANDLE m_hThread;
	DWORD m_dwThreadID;
	static DWORD StartRun(CThreadBase*);

	CThreadBase(const CThreadBase&);
	CThreadBase& operator = (const CThreadBase&);
};



#endif