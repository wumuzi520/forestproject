#ifndef FOREST_SINGLETON_H_
#define FOREST_SINGLETON_H_

template <class T>
struct CSingleton
{
	struct ObjectCreate
	{
		ObjectCreate()
		{
			CSingleton<T>::Instance();
		}
		
		inline void DoNothing() const {}
	};

	static ObjectCreate createObj;
	
	CSingleton();

public:
	typedef T ObjectType;
	static ObjectType& Instance()
	{
		static ObjectType obj;
		createObj.DoNothing();
		return obj;
	}
};

template <class T>
typename CSingleton<T>::ObjectCreate
CSingleton<T>::createObj;
	
	
/*
template <class T>
class Singleton
{
	struct object_create
	{
		object_create(){Singleton<T>::Instance();}
		inline void do_nothing() const {}
	};

	static object_create create_object;

protected:
	Singleton(){}
	virtual ~Singleton(){}
	Singleton& operator = (const Singleton&);
	Singleton(const Singleton&);

public:
	typedef T obj_type;
	static obj_type& Instance()
	{
		static obj_type obj;
 		create_object.do_nothing();
		return obj;
	}
};

template <class T>
typename Singleton<T>::object_create
Singleton<T>::create_object;
*/


#endif