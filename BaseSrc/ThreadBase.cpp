#include "stdafx.h"
#include "ThreadBase.h"

CThreadBase::CThreadBase()
	: m_hThread(0)
	, m_dwThreadID(0)
{
	m_hThread = CreateThread( NULL, 0, (LPTHREAD_START_ROUTINE )StartRun,
		this, CREATE_SUSPENDED, &m_dwThreadID);
}

CThreadBase::~CThreadBase()
{
	Close();
}

void CThreadBase::Start()
{
	ResumeThread(m_hThread);
}

void CThreadBase::Pause()
{
	SuspendThread(m_hThread);
}

void CThreadBase::Close()
{
	if(m_hThread)
	{
		CloseHandle(m_hThread);
		m_hThread = 0;
	}
}

void CThreadBase::Wait(unsigned long ms/* = INFINITE*/)
{
	WaitForSingleObject(m_hThread,ms);
}

DWORD CThreadBase::StartRun(CThreadBase* pThread)
{
	pThread->Run();

	return 0;
}

HANDLE CThreadBase::GetHandle() const
{
	return m_hThread;
}