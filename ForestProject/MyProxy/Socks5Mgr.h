#pragma once
#include <vector>
using namespace std;

#include "Socks5LinkMgr.h"
#include "RscProxyInfo.h"
#include "RscLinkMgr.h"

class CSocks5Mgr
{
private:
	CSessionListenSocket*	m_pListenSocket;
	CRscProxyInfo*			m_pRscProxyInfo;	//配置文件资源（要考虑线程同步）
	HANDLE					m_hThread;			//轮询线程句柄

public:
	CRscLinkMgr*			m_pRscLikMgr;		//LinkMgr资源（要考虑线程同步）
	BOOL					m_bDestroy;			//CSocks5Mgr销毁的标志位
	
public:
	CSocks5Mgr(void);
	~CSocks5Mgr(void);

public:
	void StartListening(UINT nPort);				//只有这一个供外部接口使用
 	void GetNextSection(ProxyInfo& nextSection);	//供下层接口调用

private:
	void OnAccept(session_event* pNotifyEvent,session_accept_data *pAcceptDetail);
	void Destroy();
};
