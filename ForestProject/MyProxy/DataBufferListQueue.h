// DataBufferListQueue.h: interface for the CDataBufferList class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATABUFFERQUEUE_H__3FB82CAD_5327_43F8_A420_A119836910D8__INCLUDED_)
#define AFX_DATABUFFERQUEUE_H__3FB82CAD_5327_43F8_A420_A119836910D8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define		BUF_NEW_FLAG	0		// 重新分别缓冲区
#define		BUF_APPEND_FLAG	1		// 增加原来的大小
struct CDataBufferObj 
{
	BYTE*		m_pBufferData;		// 指向Buffer的指针
	ULONG		m_ulBufferSize;		// Buffer大小
	ULONG		m_ulIndexOffset;	// 当前的偏移值
	
	CDataBufferObj()
	{
		m_pBufferData = NULL;		
		m_ulBufferSize = 0;		
		m_ulIndexOffset = 0;	
	}

	CDataBufferObj(CDataBufferObj &src)
	{
		*this = src;
	}
	
	virtual ~CDataBufferObj()
	{
		FreeBuffer();
	}

	BYTE* Alloc(ULONG ulBufferSize, int nFlag = BUF_NEW_FLAG)
	{
		if(nFlag != BUF_APPEND_FLAG)
		{
			FreeBuffer();
			if(ulBufferSize)
			{
				m_pBufferData = new BYTE[ulBufferSize];
				ASSERT(m_pBufferData);
				if (m_pBufferData)
				{
					memset(m_pBufferData, 0, ulBufferSize);
					m_ulBufferSize = ulBufferSize;
					m_ulIndexOffset = 0;
				}
			}
			return m_pBufferData;
		}
		else
		{
			BYTE* pTemp = new BYTE[m_ulBufferSize + ulBufferSize];
			memset(pTemp, 0, m_ulBufferSize + ulBufferSize);
			if(m_pBufferData != NULL)
			{
				memcpy(pTemp, m_pBufferData, m_ulBufferSize);
				delete[] m_pBufferData;
			}
			m_pBufferData = pTemp;
			pTemp = &m_pBufferData[m_ulBufferSize];
			m_ulBufferSize += ulBufferSize;
			return pTemp;
		}
		return NULL;
	}

	void FreeBuffer()
	{
		if(m_pBufferData)
		{
			delete []m_pBufferData;
		}
		m_pBufferData = NULL;
		m_ulBufferSize = 0;
		m_ulIndexOffset = 0;
	}

	const CDataBufferObj& operator = (const CDataBufferObj& src)
	{
		Alloc(src.m_ulBufferSize);
		if(m_pBufferData && src.m_pBufferData && src.m_ulBufferSize)
		{
			memcpy(m_pBufferData,src.m_pBufferData,src.m_ulBufferSize);
		}
		m_ulBufferSize  = src.m_ulBufferSize;		
		m_ulIndexOffset = src.m_ulIndexOffset;	
		return (*this);
	}

};


class CDataBufferListQueue : public CPtrList  
{
public:
	void MergeBuffer(CDataBufferObj& DataBufferObj);
	void RemoveBufferObj();
	void EmptyAllBufferObj();
	void RemoveBuffer();
	void AddBufferObj(CDataBufferObj* pNewBuffer);
	CDataBufferObj* GetLastBufferObj();
	CDataBufferObj* GetBufferObj();
	CDataBufferListQueue();
	virtual ~CDataBufferListQueue();
protected:
	ULONG GetTotalBufferLength();
};

#endif // !defined(AFX_DATABUFFERQUEUE_H__3FB82CAD_5327_43F8_A420_A119836910D8__INCLUDED_)
