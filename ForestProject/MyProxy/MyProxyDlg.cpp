// MyProxyDlg.cpp : implementation file
//

#include "stdafx.h"
#include "MyProxy.h"
#include "MyProxyDlg.h"
#include "Log.h"

CLog g_Log;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CMyProxyDlg dialog




CMyProxyDlg::CMyProxyDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMyProxyDlg::IDD, pParent)
	, m_nListeningPort(8086)
	, m_pSocks5Mgr(NULL)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_pSocks5Mgr = new CSocks5Mgr;
}

CMyProxyDlg::~CMyProxyDlg()
{
	SAFE_DELETE(m_pSocks5Mgr);
}

void CMyProxyDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_LISTENING_PORT, m_nListeningPort);
	DDV_MinMaxUInt(pDX, m_nListeningPort, 1, 65535);
}

BEGIN_MESSAGE_MAP(CMyProxyDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BTN_LISTEN, &CMyProxyDlg::OnBnClickedBtnListen)
	ON_BN_CLICKED(IDC_BTN_CANCEL, &CMyProxyDlg::OnBnClickedBtnCancel)
END_MESSAGE_MAP()


// CMyProxyDlg message handlers

BOOL CMyProxyDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	g_Log.SetWindow(GetDlgItem(IDC_EDIT_LOG)->GetSafeHwnd());

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CMyProxyDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CMyProxyDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CMyProxyDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CMyProxyDlg::OnBnClickedBtnListen()
{
	// TODO: Add your control notification handler code here
	UpdateData();

	if(m_nListeningPort >=1 && m_nListeningPort <= 65535)
	{
		if(m_pSocks5Mgr)
		{
			m_pSocks5Mgr->StartListening(m_nListeningPort);
		}
		GetDlgItem(IDC_BTN_LISTEN)->EnableWindow(FALSE);
	}
}

void CMyProxyDlg::OnBnClickedBtnCancel()
{
	// TODO: Add your control notification handler code here
	OnCancel();
}
