#pragma once
#include "Socks5Link.h"
#include "ProxyInfo.h"

class CSocks5AppToSrvLink :
	public CSocks5Link
{
private:
	ConsulationState		m_enumStateReq;			//APP请求状态接收

public:
	CSocks5AppToSrvLink(void);
	virtual ~CSocks5AppToSrvLink(void);

public:
	void BindEventHandle();
	void OnConnectSucc(session_event* pNotifyEvent);
	void OnConnectFail(session_event* pNotifyEvent,session_connect_fail *pFailDetail);
	void OnDisconnect(session_event* pNotifyEvent,session_disconnect *pDisDetail);
	void OnSeverRecvingData( session_event* pNotifyEvent,session_received_data *pRecvDetail );

public:
	void OnAccept( session_event* pNotifyEvent,session_accept_data *pAcceptDetail );	//用于接受App链接请求
	void TransDataToAppSrv(LPBYTE pData,ULONG nLen);
	void OnSuccess();
// 
// 	void SendData(LPBYTE pData,ULONG nLen)
// 	{
// 		TRACE("App<-------------MyClient（发）\n");
// 		m_pSessionLink->Send(pData,nLen);
// 	}

	void GetAppAddr(CString& strIP);

protected:
	BOOL CheckReq(LPBYTE pData, ULONG nLen);
};
