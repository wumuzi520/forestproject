#pragma once
#include "Socks5Link.h"
#include "ProxyInfo.h"

class CSocks5ClientToAppSrvLink :
	public CSocks5Link
{
private:
	ConsulationState		m_enumStateReq;			//APP请求状态接收

public:
	CSocks5ClientToAppSrvLink(void);
	virtual ~CSocks5ClientToAppSrvLink(void);

public:
	void BindEventHandle();
	void OnConnectSucc(session_event* pNotifyEvent);
	void OnConnectFail(session_event* pNotifyEvent,session_connect_fail *pFailDetail);
	void OnDisconnect(session_event* pNotifyEvent,session_disconnect *pDisDetail);
	void OnClientRecvingData( session_event* pNotifyEvent,session_received_data *pRecvDetail );


public:
	void TransDataToApp(LPBYTE pData, ULONG nLen);
	void CreateConnectToAppSrv(CString strAppSrv,UINT nPort);
// 	void SendData(LPBYTE pData,ULONG nLen)
// 	{
// 		TRACE("MyClient（发）------------->代理\n");
// 		m_pSessionLink->Send(pData,nLen);
// 	}

protected:
	BOOL CheckRep(LPBYTE pData, ULONG nLen);
};
