#include "StdAfx.h"
#include "Socks5AppToSrvLink.h"
#include "Socks5LinkMgr.h"
#include "SessionSocket.h"

CSocks5AppToSrvLink::CSocks5AppToSrvLink(void)
{
	m_enumStateReq = CONS_INIT;
}

CSocks5AppToSrvLink::~CSocks5AppToSrvLink(void)
{
}

void CSocks5AppToSrvLink::BindEventHandle()
{
	if(m_pSessionLink)
	{
		m_pSessionLink->m_ConnectSuccEventHandler.bind(this,&CSocks5AppToSrvLink::OnConnectSucc);
		m_pSessionLink->m_ConnectFailEventHandler.bind(this,&CSocks5AppToSrvLink::OnConnectFail);
		m_pSessionLink->m_DisconnectEventHandler.bind(this,&CSocks5AppToSrvLink::OnDisconnect);
		m_pSessionLink->m_RecvingDataEventHandler.bind(this,&CSocks5AppToSrvLink::OnSeverRecvingData);
	}
}

void CSocks5AppToSrvLink::OnConnectSucc(session_event* pNotifyEvent)
{
	TRACE("Srv连接成功！SessionID = %d \r\n",pNotifyEvent->m_dwSessionID);
	m_enumStateReq = FIRST_REQ;		//期待第一次请求
}

void CSocks5AppToSrvLink::OnConnectFail(session_event* pNotifyEvent,session_connect_fail *pFailDetail)
{
	TRACE("!!!!!连接出错！%s\r\n",pFailDetail->m_strErrorMessage);
}

void CSocks5AppToSrvLink::OnDisconnect(session_event* pNotifyEvent,session_disconnect *pDisDetail)
{
	CString strDisMode("未知");
	switch(pDisDetail->m_dwDisconnectMode)
	{
	case SESSION_CLOSE_REMOTE:
		{
			strDisMode = "远程App";
			//App断开连接后，通知MyClient和AppSrv的连接
			if(m_pMgr)
			{
				//记录断开日志
				g_Log.UnRegisterSessionLink(m_pMgr->m_strSrcIP,m_pMgr->m_strDstIP);	

				m_pMgr->NotifyAppSrvDisconnect();
			}
		}
		break;
	case SESSION_CLOSE_CLIENT:
		{
			strDisMode = "本地";
		}
		break;
	case SESSION_CLOSE_CONNECT_TIMEOUT:
		{
			strDisMode = "超时原因";
		}
		break;
	case SESSION_CLOSE_SOCKET_ERROR:
		{
			strDisMode = "Socket错误原因";			
		}
		break;
	}
	TRACE("%s发起的连接断开 SessionID = %d \r\n",strDisMode,pNotifyEvent->m_dwSessionID);
}

void CSocks5AppToSrvLink::OnSeverRecvingData( session_event* pNotifyEvent,session_received_data *pRecvDetail )
{
	switch(m_enumStateReq)
	{
	case FIRST_REQ:	//第一次请求到来
		{
			TRACE("APP-------->MySever(FIRST_REQ 收)\n");
			
			if(CheckReq(pRecvDetail->m_pszBuffer,pRecvDetail->m_lTransLength))
			{
				SendData(m_pBuf,2);
				m_enumStateReq = SECOND_REQ; //期待第二次请求
			}
			else
			{
				TRACE("Check FIRST_REQ Error!\n");
			}
		}
		break;
	case SECOND_REQ:
		{
			TRACE("APP-------->MySever(SECOND_REQ 收)\n");
			if(CheckReq(pRecvDetail->m_pszBuffer,pRecvDetail->m_lTransLength))
			{
				SendData(m_pBuf,2);
				m_enumStateReq = THIRD_REQ; //期待第三次请求
			}
			else
			{
				TRACE("Check SECOND_REQ Error!\n");
			}
		}
		break;
	case THIRD_REQ:
		{
			TRACE("APP-------->MySever(THIRD_REQ 收)\n");
			if(CheckReq(pRecvDetail->m_pszBuffer,pRecvDetail->m_lTransLength))
			{
				//创建Right到AppSrv的连接
				if(m_pMgr)
				{
					m_pMgr->CreateConnectToAppSrv();
				}
			}
			else
			{
				TRACE("Check THIRD_REQ Error!\n");
			}
		}
		break;
	case CONS_DONE:
		{
			TransDataToAppSrv(pRecvDetail->m_pszBuffer,pRecvDetail->m_lTransLength);
		}
		break;
	default:
		break;
	}

}

BOOL CSocks5AppToSrvLink::CheckReq(LPBYTE pData, ULONG nLen)
{
	if(m_enumStateReq == FIRST_REQ)
	{
		if(nLen < 3)
			return FALSE;
		if(pData[0] != SOCKS_VER)
			return FALSE;
		int length = pData[1];
		int i = 0;
		for (; i < length; i++)
		{
			if(*(pData+i+2) == 0x02)
				break;
		}

		m_pBuf[0] = SOCKS_VER;
		if(i == length)
			m_pBuf[1] = METHOD_NONE;
		else
		{
			m_pBuf[1] = METHOD_AUTH;

			//修改并存储本次请求信息
			BYTE chFirstReq[3] = {0};
			if(m_pMgr)
				m_pMgr->SaveReqInfo(FIRST_REQ,chFirstReq,3);
		}

		return TRUE;
	}
	else if(m_enumStateReq == SECOND_REQ)
	{
		if(pData[0] != SOCKS_AUTH_VER)
			return FALSE;
		//不用检查用户名密码，在right到AppSrv连接时读取配置文件中的用户名密码
		m_pBuf[0] = SOCKS_AUTH_VER;
		m_pBuf[1] = REP_SUCCESS;
		return TRUE;
	}
	else if(m_enumStateReq == THIRD_REQ)
	{
		if(pData[0] != SOCKS_VER)
			return FALSE;

		if(pData[1] != CMD_CONNECT)
			return FALSE;

		if(pData[2] != FIELD_RSV)
			return FALSE;

		BYTE chThirdReq[256] = {0};

		memcpy(chThirdReq,pData,nLen);
		if(m_pMgr)
			m_pMgr->SaveReqInfo(THIRD_REQ,chThirdReq,nLen);

		return TRUE;
	}

	return FALSE;
}

void CSocks5AppToSrvLink::OnAccept( session_event* pNotifyEvent,session_accept_data *pAcceptDetail )
{
	TRACE("CSocks5ProxyServerLink::Accept\n");
	if(pAcceptDetail && 
		(pAcceptDetail->m_nErrorCode == 0) && 
		pAcceptDetail->m_pListenSocket)
	{
		if (m_pSessionLink == NULL)
			m_pSessionLink = new CSessionLink;
		else
			m_pSessionLink->Disconnect();

		BindEventHandle();

		m_pSessionLink->OnAccept(pAcceptDetail->m_pListenSocket);
	}
}

void CSocks5AppToSrvLink::OnSuccess()
{
	m_enumStateReq = CONS_DONE;

	//记录连接日志
	if(m_pMgr)
	{
		g_Log.RegisterSessionLink(m_pMgr->m_strSrcIP,m_pMgr->m_strDstIP);
	}
}

void CSocks5AppToSrvLink::TransDataToAppSrv(LPBYTE pData,ULONG nLen)
{
	if(m_pMgr)
	{
		m_pMgr->TransDataToAppSrv(pData,nLen);
	}
}


void CSocks5AppToSrvLink::GetAppAddr(CString& strIP)
{
	if(m_pSessionLink)
	{
		SOCKADDR_IN addr;
		int nLen = sizeof(SOCKADDR);
		m_pSessionLink->m_pDataSocket->GetPeerName((SOCKADDR*)&addr,&nLen);
		strIP = inet_ntoa(addr.sin_addr);
	}
}