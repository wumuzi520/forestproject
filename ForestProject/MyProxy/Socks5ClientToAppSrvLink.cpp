#include "StdAfx.h"
#include "Socks5ClientToAppSrvLink.h"
#include "Socks5LinkMgr.h"


CSocks5ClientToAppSrvLink::CSocks5ClientToAppSrvLink(void)
{
	m_enumStateReq = CONS_INIT;
}

CSocks5ClientToAppSrvLink::~CSocks5ClientToAppSrvLink(void)
{
}


void CSocks5ClientToAppSrvLink::BindEventHandle()
{
	if(m_pSessionLink)
	{
		m_pSessionLink->m_ConnectSuccEventHandler.bind(this,&CSocks5ClientToAppSrvLink::OnConnectSucc);
		m_pSessionLink->m_ConnectFailEventHandler.bind(this,&CSocks5ClientToAppSrvLink::OnConnectFail);
		m_pSessionLink->m_DisconnectEventHandler.bind(this,&CSocks5ClientToAppSrvLink::OnDisconnect);
		m_pSessionLink->m_RecvingDataEventHandler.bind(this,&CSocks5ClientToAppSrvLink::OnClientRecvingData);
	}
}

void CSocks5ClientToAppSrvLink::OnConnectFail(session_event* pNotifyEvent,session_connect_fail *pFailDetail)
{
	TRACE("!!!!!连接出错！%s\r\n",pFailDetail->m_strErrorMessage);
}

void CSocks5ClientToAppSrvLink::OnDisconnect(session_event* pNotifyEvent,session_disconnect *pDisDetail)
{
	CString strDisMode("未知");
	switch(pDisDetail->m_dwDisconnectMode)
	{
	case SESSION_CLOSE_REMOTE:
		{
			strDisMode = "远程Proxy";
			//Proxy断开后通知MySrv断开和App的连接
			if(m_pMgr)
			{	
				//记录断开日志
				g_Log.UnRegisterSessionLink(m_pMgr->m_strSrcIP,m_pMgr->m_strDstIP);

				m_pMgr->NotifyAppDisconnect();
			}
		}
		break;
	case SESSION_CLOSE_CLIENT:
		{
			strDisMode = "本地";
		}
		break;
	case SESSION_CLOSE_CONNECT_TIMEOUT:
		{
			strDisMode = "超时原因";
		}
		break;
	case SESSION_CLOSE_SOCKET_ERROR:
		{
			strDisMode = "Socket错误原因";						
		}
		break;
	}
	TRACE("%s发起的连接断开 SessionID = %d \r\n",strDisMode,pNotifyEvent->m_dwSessionID);
}

void CSocks5ClientToAppSrvLink::OnConnectSucc(session_event* pNotifyEvent)
{
	TRACE("Client连接成功！SessionID = %d \r\n",pNotifyEvent->m_dwSessionID);
	//链接成功，转发存储在Mgr中的Methods信息
	if(m_pMgr)
	{
		SendData(m_pMgr->m_pMethodsReq,3);

		m_enumStateReq = FIRST_REP;	//期待第一次回应
	}
}

void CSocks5ClientToAppSrvLink::OnClientRecvingData( session_event* pNotifyEvent,session_received_data *pRecvDetail )
{
	switch(m_enumStateReq)
	{
	case FIRST_REP:
		{
			TRACE("<--------------FIRST_REP(代理回应)\n");
			if(CheckRep(pRecvDetail->m_pszBuffer,pRecvDetail->m_lTransLength))
			{
				//转发Grant消息
				if(m_pMgr)
				{
					SendData(m_pMgr->m_pGrantReq+1,*(m_pMgr->m_pGrantReq));
					m_enumStateReq  = SECOND_REP;	//期待第二次回应
				}
			}
			else
			{
				TRACE("CheckRep FIRST_REP Error!\n");
				//通知MySrv端断开与App的连接
				if(m_pMgr)
				{
					m_pMgr->NotifyAppDisconnect();
				}
			}
		}
		break;
	case SECOND_REP:
		{
			TRACE("<--------------SECOND_REP(代理回应)\n");
			if(CheckRep(pRecvDetail->m_pszBuffer,pRecvDetail->m_lTransLength))
			{
				if(m_pMgr)
				{
					SendData(m_pMgr->m_pCMDReq+1,*(m_pMgr->m_pCMDReq));
					m_enumStateReq  = CONS_DONE;	//期待协商Success回应
				}
			}
			else
			{
				TRACE("CheckRep SECOND_REP Error!\n");
				//通知MySrv端断开与App的连接
				if(m_pMgr)
				{
					m_pMgr->NotifyAppDisconnect();
				}
			}
		}
		break;
	case CONS_DONE:
		{
			//MyClient和AppSrv协商成功后，通知MySrv端协商成功
			if(m_pMgr  && !m_pMgr->m_bIsSuccess)
			{
				m_pMgr->m_bIsSuccess = TRUE;
				m_pMgr->NotifyAppSuccess();
			}

			//开始转发数据
			TransDataToApp(pRecvDetail->m_pszBuffer,pRecvDetail->m_lTransLength);
		}
		break;
	default:
		break;
	}
}

BOOL CSocks5ClientToAppSrvLink::CheckRep(LPBYTE pData, ULONG nLen)
{
	if(m_enumStateReq == FIRST_REP)
	{
		if(pData[0] != SOCKS_VER)
			return FALSE;
		if(pData[1] == METHOD_AUTH)
			return TRUE;
		else if(pData[1] == METHOD_NONE)
		{
			//通知MySrv端和App断开连接
			if(m_pMgr)
			{
				m_pMgr->NotifyAppDisconnect();
			}
		}

		return FALSE;
	}
	else if(m_enumStateReq == SECOND_REP)
	{
		if(pData[0] != SOCKS_AUTH_VER)
			return FALSE;

		if(pData[1] == REP_SUCCESS)
			return TRUE;
		else
		{
			//通知MySrv端和App断开连接
			if(m_pMgr)
			{
				m_pMgr->NotifyAppDisconnect();
			}
		}

		return FALSE;
	}

	return FALSE;
}


void CSocks5ClientToAppSrvLink::TransDataToApp(LPBYTE pData, ULONG nLen)
{
	TRACE("App(收)<----------------代理\n");
	if(m_pMgr)
	{
		m_pMgr->TransDataToApp(pData,nLen);
	}
}

void CSocks5ClientToAppSrvLink::CreateConnectToAppSrv(CString strAppSrv,UINT nPort)
{
	if(m_pSessionLink)
	{
		m_pSessionLink->Disconnect();
	}
	else
	{
		m_pSessionLink = new CSessionLink;
	}

	// 将Session处理绑定
	BindEventHandle();

	m_pSessionLink->Connect(strAppSrv,nPort);
}
