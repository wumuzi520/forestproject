#pragma once
#include "SafeLock.h"
#include "SessionLink.h"
#include <map>
using namespace std;

class CLog
{
private:
	CSafeLock* m_pLock;
	CString m_strLog;
	HWND	m_hWnd;

	CSafeLock*	m_pRegistLock;
	map<CString, UINT> m_MapInfo;

public:
	CLog(void);
	~CLog(void);

public:
	void SetWindow(HWND hWnd);
	void ReadLog(CString& strLog);

#ifdef _DEBUG 
#if _UNICODE 
	void AppendLog(LPCWSTR str,...);
#else 
	void AppendLog(LPCTSTR str,...);
#endif 
#else 
#define WTrace NULL 
#endif

public:
	void RegisterSessionLink(CString strSrcIP, CString strDstIP);
	void UnRegisterSessionLink(CString strSrcIP, CString strDstIP);

private:
	void ShowLog();
};
