// MyProxyDlg.h : header file
//

#pragma once
#include "Socks5Mgr.h"

// CMyProxyDlg dialog
class CMyProxyDlg : public CDialog
{
// Construction
public:
	CMyProxyDlg(CWnd* pParent = NULL);	// standard constructor
	virtual ~CMyProxyDlg();

// Dialog Data
	enum { IDD = IDD_MYPROXY_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support


// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	UINT m_nListeningPort;
public:
	afx_msg void OnBnClickedBtnListen();
public:
	afx_msg void OnBnClickedBtnCancel();

private:
	CSocks5Mgr*		m_pSocks5Mgr;
};
