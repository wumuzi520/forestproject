// SessionListenSocket.cpp : implementation file
//

#include "stdafx.h"
#include "SessionListenSocket.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSessionListenSocket

CSessionListenSocket::CSessionListenSocket()
{
	InitMember();
}

CSessionListenSocket::CSessionListenSocket( UINT nPort )
{
	InitMember();
	m_uListenPort = nPort;
}

void CSessionListenSocket::InitMember()
{
	m_bListened = FALSE;
	m_uListenPort = 0;
	m_AcceptEventHandler = NULL;
}

CSessionListenSocket::~CSessionListenSocket()
{
	
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CSessionListenSocket, CAsyncSocket)
	//{{AFX_MSG_MAP(CSessionListenSocket)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/*!===============================================================
  函数功能 : 开始监听用这个函数

  创建信息 : 2007年11月10日  By ZhangYuBo HEXIN Software Co., Ltd. 
	
  历史记录 : 
================================================================*/
BOOL CSessionListenSocket::SessionListen()
{
	BOOL bRet = FALSE;
	if(m_bListened == FALSE)
	{
		if(m_uListenPort > 0)
		{
			bRet = Create(m_uListenPort,SOCK_STREAM,FD_ACCEPT);
			if(bRet)
			{
				bRet = Listen();
				if(bRet)
				{
					m_bListened = TRUE;
				}
			}
		}
	}
	return bRet;
}

void CSessionListenSocket::InitEventType(session_event &event, DWORD dwType)
{
	event.m_dwSessionID = m_uListenPort;
	event.m_dwEventType = dwType;
}

void CSessionListenSocket::OnAccept(int nErrorCode) 
{
	// TODO: Add your specialized code here and/or call the base class
	session_event event;
	InitEventType(event,SESSION_EVENT_ACCEPT);
	
	session_accept_data _accept;
	_accept.m_nErrorCode = nErrorCode;
	_accept.m_pListenSocket = this;

	if(m_AcceptEventHandler)
	{
		m_AcceptEventHandler(&event,&_accept);
	}
}

UINT CSessionListenSocket::GetListenPort()
{
	return m_uListenPort;
}

void CSessionListenSocket::SetListenPort( UINT nPort )
{
	m_uListenPort = nPort;	
}

BOOL CSessionListenSocket::IsListenState()
{
	return m_bListened;
}
