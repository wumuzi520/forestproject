#include "StdAfx.h"
#include "RscLinkMgr.h"

CRscLinkMgr::CRscLinkMgr(void)
{
	m_pLock = new CSafeLock;
}

CRscLinkMgr::~CRscLinkMgr(void)
{
	Destroy();
}

void CRscLinkMgr::QueryItem()
{
	m_pLock->Lock();
	for(vector<CSocks5LinkMgr*>::iterator iter = m_VecLinkMgr.begin();
		iter != m_VecLinkMgr.end(); iter++)
	{
		//轮询标志位
		if(*iter != NULL && (*iter)->m_bIsDisconnect)
		{
			TRACE("****************清除无用的空间*************\n");
			SAFE_DELETE(*iter);
		}
	}
	m_pLock->UnLock();
}

void CRscLinkMgr::AddItem(CSocks5LinkMgr* pSocks5LinkMgr)
{
	m_pLock->Lock();
	vector<CSocks5LinkMgr*>::iterator iter = m_VecLinkMgr.begin();
	for (;iter != m_VecLinkMgr.end(); iter++)
	{
		if(*iter == NULL)
		{
			*iter = pSocks5LinkMgr;
			break;
		}
	}
	if(iter == m_VecLinkMgr.end())
		m_VecLinkMgr.push_back(pSocks5LinkMgr);
	m_pLock->UnLock();
}


void CRscLinkMgr::Destroy()
{
	m_pLock->Lock();
	for(vector<CSocks5LinkMgr*>::iterator iter = m_VecLinkMgr.begin();
		iter != m_VecLinkMgr.end(); iter++)
	{
		SAFE_DELETE(*iter);
	}
	m_VecLinkMgr.clear();
	m_pLock->UnLock();

	SAFE_DELETE(m_pLock);
}