#pragma once
#include "stdafx.h"
#include "SafeLock.h"

CSafeLock::CSafeLock()
{
	InitializeCriticalSection(&m_Lock);
}

CSafeLock::~CSafeLock()
{

}

void CSafeLock::Lock()
{
	EnterCriticalSection(&m_Lock);
}

void CSafeLock::UnLock()
{
	LeaveCriticalSection(&m_Lock);
}