#include "StdAfx.h"
#include "Socks5Link.h"
#include "Socks5LinkMgr.h"
#include "SessionSocket.h"

CSocks5Link::CSocks5Link(void)
{
	m_pMgr = NULL;
	m_pSessionLink = NULL;
	m_pBuf = NULL;
}

CSocks5Link::~CSocks5Link(void)
{
	Destory();
}

void CSocks5Link::Destory()
{
	if(m_pSessionLink)
	{
		m_pSessionLink->Disconnect();
		delete m_pSessionLink;
		m_pSessionLink = NULL;
	}

	this->DetachMgr();
	this->DetachMgrBuf();
}

void CSocks5Link::AttachMgr(CSocks5LinkMgr* pMgr)
{
	if(m_pMgr)
	{
		this->DetachMgr();
	}

	m_pMgr = pMgr;
}

void CSocks5Link::DetachMgr()
{
	m_pMgr = NULL;
}

void CSocks5Link::AttachMgrBuf(LPBYTE pBuf)
{
	if(m_pBuf)
	{
		this->DetachMgrBuf();
	}
	m_pBuf = pBuf;
}

void CSocks5Link::DetachMgrBuf()
{
	m_pBuf = NULL;
}

void CSocks5Link::SendData(LPBYTE pData,ULONG nLen)
{
	m_pSessionLink->Send(pData,nLen);
}

void CSocks5Link::DoDisconnect()
{
	if(m_pSessionLink)
	{
		m_pSessionLink->Disconnect();
		SAFE_DELETE(m_pSessionLink);
	}

	//通知设置上级Disconnect标志，由Socks5Mgr回收
	if(m_pMgr)
	{
		m_pMgr->OnDisconnect();
	}
}