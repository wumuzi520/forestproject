#include "StdAfx.h"
#include "RscProxyInfo.h"

CRscProxyInfo::CRscProxyInfo(CString strINIPath)
{
	m_nIndex = 0;
	m_pLock = new CSafeLock;

	FromINI(strINIPath);
}

CRscProxyInfo::~CRscProxyInfo(void)
{
	Destroy();
}

void CRscProxyInfo::Destroy()
{
	m_pLock->Lock();
	for (vector<ProxyInfo*>::iterator iter = m_VecProxyInfo.begin();
		iter != m_VecProxyInfo.end(); iter++)
	{
		SAFE_DELETE(*iter);
	}
	m_VecProxyInfo.clear();
	m_pLock->UnLock();

	SAFE_DELETE(m_pLock);
}

void CRscProxyInfo::GetNextSection(ProxyInfo& nextSection)
{
	m_pLock->Lock();

	if(m_VecProxyInfo.size() != 0)
	{
		if(m_nIndex == m_VecProxyInfo.size())
		{
			m_nIndex %= m_VecProxyInfo.size();
		}

		nextSection.strProxyIP = m_VecProxyInfo[m_nIndex]->strProxyIP;
		nextSection.nProxyPort = m_VecProxyInfo[m_nIndex]->nProxyPort;
		nextSection.strUserName = m_VecProxyInfo[m_nIndex]->strUserName;
		nextSection.strPassword = m_VecProxyInfo[m_nIndex]->strPassword;
		nextSection.Seq = m_VecProxyInfo[m_nIndex]->Seq;
		nextSection.UseState = m_VecProxyInfo[m_nIndex]->UseState;

		m_nIndex++;
	}

	m_pLock->UnLock();
}

void CRscProxyInfo::FromINI(CString path)
{
	int numOfSection;
	vector<CString> section;
	numOfSection = CalcCount(path,section);
	for (int i = 0;i<numOfSection;i++)
	{
		CString m_addstr;
		int m_port;
		CString m_user;
		CString m_password;

		ProxyInfo* theVector = new ProxyInfo;
		GetPrivateProfileString(section[i],"addrStr",CString("NULL"),m_addstr.GetBuffer(100),100,path);
		m_port = GetPrivateProfileInt(section[i],"port",0,path);
		GetPrivateProfileString(section[i],"user",CString("NULL"),m_user.GetBuffer(256),256,path);
		GetPrivateProfileString(section[i],"password",CString("NULL"),m_password.GetBuffer(256),256,path);
		theVector->strProxyIP = m_addstr;
		theVector->nProxyPort = m_port;
		theVector->strUserName = m_user;
		theVector->strPassword = m_password;
		theVector->Seq = i;
		theVector->UseState = 0;
		m_VecProxyInfo.push_back(theVector);
		m_addstr.ReleaseBuffer();
		m_user.ReleaseBuffer();
		m_password.ReleaseBuffer();
	}
}


int CRscProxyInfo::CalcCount(CString path , vector<CString> &section)
{
	TCHAR       chSectionNames[2048]={0};       //所有节名组成的字符数组
	char * pSectionName; //保存找到的某个节名字符串的首地址
	int i;       //i指向数组chSectionNames的某个位置，从0开始，顺序后移
	int j=0;      //j用来保存下一个节名字符串的首地址相对于当前i的位置偏移量
	int count=0;      //统计节的个数


	GetPrivateProfileSectionNames(chSectionNames,2048,path); 

	for(i=0;i<2048;i++,j++)
	{
		if(chSectionNames[0]=='\0')
			break;       //如果第一个字符就是0，则说明ini中一个节也没有

		if(chSectionNames[i]=='\0')
		{
			pSectionName=&chSectionNames[i-j]; //找到一个0，则说明从这个字符往前，减掉j个偏移量，
			//就是一个节名的首地址
			j=-1;         //找到一个节名后，j的值要还原，以统计下一个节名地址的偏移量
			//赋成-1是因为节名字符串的最后一个字符0是终止符，不能作为节名
			//的一部分
			//在获取节名的时候可以获取该节中键的值，前提是我们知道该节中有哪些键。
			//		printf("%s\n",pSectionName);
			section.push_back(pSectionName);

			count++;

			if(chSectionNames[i+1]==0)
				break;      //当两个相邻的字符都是0时，则所有的节名都已找到，循环终止
		} 
	}

	return count;
}