#include "StdAfx.h"
#include "Socks5LinkMgr.h"
#include "Socks5Mgr.h"


CSocks5LinkMgr::CSocks5LinkMgr(void)
{
	m_pSocks5AppToSrv = new CSocks5AppToSrvLink;
	m_pSocks5ClientToAppSrv = NULL /*new CSocks5ClientToAppSrvLink*/;
	m_pSocks5Mgr = NULL;

	m_pBuf = new BYTE[BUFFER_SIZE];		//该Buf为左右两端共同使用
	memset(m_pBuf,0,BUFFER_SIZE);
	
	m_pMethodsReq = new BYTE[3];
	memset(m_pMethodsReq,0,3);

	m_pGrantReq = new BYTE[256];
	memset(m_pGrantReq,0,256);
	

	m_pCMDReq = new BYTE[255];
	memset(m_pCMDReq,0,255);

	m_bIsDisconnect = FALSE;

	m_strDstIP = "";
	m_strSrcIP = "";
	m_bIsSuccess = FALSE;

	m_pSocks5AppToSrv->AttachMgr(this);
	m_pSocks5AppToSrv->AttachMgrBuf(m_pBuf);
}

CSocks5LinkMgr::~CSocks5LinkMgr(void)
{
	Destroy();
}

void CSocks5LinkMgr::Destroy()
{
	SAFE_DELETE(m_pSocks5AppToSrv);
	SAFE_DELETE(m_pSocks5ClientToAppSrv);

// 	if(m_pGrantReq)
// 	{
// 		delete [] m_pGrantReq;
// 		m_pGrantReq = NULL;
// 	}
// 	if(m_pCMDReq)
// 	{
// 		delete [] m_pCMDReq;
// 		m_pCMDReq = NULL;
// 	}
// 	if(m_pMethodsReq)
// 	{
// 		delete [] m_pMethodsReq;
// 		m_pMethodsReq = NULL;
// 	}

	MemRecovery();

	if(m_pBuf)
	{
		delete [] m_pBuf;
		m_pBuf = NULL;
	}


	if(m_pSocks5Mgr)
	{
		this->DetachSocks5Mgr();	
	}
}

void CSocks5LinkMgr::MemRecovery()
{
	if(m_pGrantReq)
	{
		delete [] m_pGrantReq;
		m_pGrantReq = NULL;
	}
	if(m_pCMDReq)
	{
		delete [] m_pCMDReq;
		m_pCMDReq = NULL;
	}
	if(m_pMethodsReq)
	{
		delete [] m_pMethodsReq;
		m_pMethodsReq = NULL;
	}
}

void CSocks5LinkMgr::SaveReqInfo(ConsulationState enumState, LPBYTE pData, ULONG nLen)
{
	if(enumState == FIRST_REQ)
	{
		m_pMethodsReq[0] = SOCKS_VER;
		m_pMethodsReq[1] = 1;
		m_pMethodsReq[2] = METHOD_AUTH;
	}
	else if(enumState == SECOND_REQ)
	{
		//不会进入这段代码，这部分由GetConfigInfo设定
	}
	else if(enumState == THIRD_REQ)
	{
		//本程序约定,该buf中第一个即m_pGrantReq[0]存放的是本信息的长度,真实信息从m_pGrantReq[1]开始存放
		m_pCMDReq[0] = (BYTE)nLen;
		memcpy(m_pCMDReq+1,pData,nLen);

		//此处保存目的地址
		m_strDstIP.Format("%d.%d.%d.%d",pData[4],pData[5],pData[6],pData[7]);
	}
}

void CSocks5LinkMgr::AttachSocks5Mgr(CSocks5Mgr* pMgr)
{
	m_pSocks5Mgr = pMgr;
}

void CSocks5LinkMgr::DetachSocks5Mgr()
{
	if(m_pSocks5Mgr)
	{
		m_pSocks5Mgr = NULL;
	}
}

void CSocks5LinkMgr::TransDataToApp(LPBYTE pData, ULONG nLen)
{
	if(m_pSocks5AppToSrv)
	{
		m_pSocks5AppToSrv->SendData(pData,nLen);
	}
}

void CSocks5LinkMgr::TransDataToAppSrv(LPBYTE pData, ULONG nLen)
{
	TRACE("App(发)---------------->代理\n");
	if(m_pSocks5ClientToAppSrv)
	{
		m_pSocks5ClientToAppSrv->SendData(pData,nLen);
	}
}

void CSocks5LinkMgr::OnAccept(session_event* pNotifyEvent,session_accept_data *pAcceptDetail)
{
	if(m_pSocks5AppToSrv)
	{
		m_pSocks5AppToSrv->OnAccept(pNotifyEvent,pAcceptDetail);
	}
}

void CSocks5LinkMgr::CreateConnectToAppSrv()
{
	SAFE_DELETE(m_pSocks5ClientToAppSrv);
	m_pSocks5ClientToAppSrv = new CSocks5ClientToAppSrvLink;
	m_pSocks5ClientToAppSrv->AttachMgr(this);
	m_pSocks5ClientToAppSrv->AttachMgrBuf(m_pBuf);
// 	if(m_pSocks5ClientToAppSrv == NULL)
// 	{
// 		m_pSocks5ClientToAppSrv = new CSocks5ClientToAppSrvLink;
// 		m_pSocks5ClientToAppSrv->AttachMgr(this);
// 		m_pSocks5ClientToAppSrv->AttachMgrBuf(m_pBuf);
// 	}

	//获取配置文件中下一个代理服务器ip及端口号
	CString strAppSrv = "";
	UINT nPort = 0;
	UINT nSeq = 0;
	GetConfigInfo(strAppSrv,nPort,nSeq);
	
	TRACE("%s,%d,%d\n",strAppSrv,nPort,nSeq);

	if(m_pSocks5ClientToAppSrv)
	{
		m_pSocks5ClientToAppSrv->CreateConnectToAppSrv(strAppSrv,nPort);
	}
}


void CSocks5LinkMgr::GetConfigInfo(CString& strAppSrv,UINT& nPort,UINT& nSeq)
{
	ProxyInfo* pInfo = new ProxyInfo;
	m_pSocks5Mgr->GetNextSection(*pInfo);

	//提取Srv地址及端口号
	strAppSrv = pInfo->strProxyIP;
	nPort = pInfo->nProxyPort;
	nSeq = pInfo->Seq;

	//本程序约定,该buf中第一个即m_pGrantReq[0]存放的是本信息的长度,真实信息从m_pGrantReq[1]开始存放
	//设定要发送的用户名及密码
	m_pGrantReq[1] = SOCKS_AUTH_VER;
	
	int nNameLen = pInfo->strUserName.GetLength();
	m_pGrantReq[2] = nNameLen;
	
	memcpy(m_pGrantReq+3,pInfo->strUserName.GetBuffer(),nNameLen);
	
	int nPasswLen = pInfo->strPassword.GetLength();
	m_pGrantReq[nNameLen+3] = nPasswLen;
	
	memcpy(m_pGrantReq+nNameLen+4,pInfo->strPassword.GetBuffer(),nPasswLen);

	//紧接着下一个字节存放该信息的长度，这是本程序约定
	m_pGrantReq[0] = nNameLen+nPasswLen+3;

	delete pInfo;
}

void CSocks5LinkMgr::GetAppAddr(CString& strIP)
{
	if(m_pSocks5AppToSrv)
	{
		m_pSocks5AppToSrv->GetAppAddr(strIP);
	}
}


void CSocks5LinkMgr::NotifyAppSuccess()
{
	GetAppAddr(m_strSrcIP);

	MemRecovery();//连接成功则回收存放转发信息的内存

	if(m_pSocks5AppToSrv)
	{
		m_pSocks5AppToSrv->OnSuccess();
	}
}

void CSocks5LinkMgr::NotifyAppSrvDisconnect()
{
	if(m_pSocks5ClientToAppSrv)
	{
		m_pSocks5ClientToAppSrv->DoDisconnect();
	}
}

void CSocks5LinkMgr::NotifyAppDisconnect()
{
	if(m_pSocks5AppToSrv)
	{
		m_pSocks5AppToSrv->DoDisconnect();
	}
}

void CSocks5LinkMgr::OnDisconnect()
{
	m_bIsDisconnect = TRUE;
}
