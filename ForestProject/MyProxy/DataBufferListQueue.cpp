// DataBufferListQueue.cpp: implementation of the CDataBufferList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "DataBufferListQueue.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CDataBufferListQueue::CDataBufferListQueue()
{

}

CDataBufferListQueue::~CDataBufferListQueue()
{
	EmptyAllBufferObj();
}

CDataBufferObj* CDataBufferListQueue::GetLastBufferObj()
{
	CDataBufferObj* pBufferObj = NULL;
	pBufferObj = (CDataBufferObj*)GetTail();
	return pBufferObj;
}
/*!===============================================================
  函数功能 : 得到缓冲链表中的第一个可用的缓冲区对象

  创建信息 : 2007年11月7日  By ZhangYuBo HEXIN Software Co., Ltd. 
	
  历史记录 : 
================================================================*/
CDataBufferObj* CDataBufferListQueue::GetBufferObj()
{
	CDataBufferObj* pBufferObj = NULL;
	pBufferObj = (CDataBufferObj*)GetHead();
	return pBufferObj;
}

/*!===============================================================
  函数功能 : 向链表中增加一个缓冲区对象

  创建信息 : 2007年11月7日  By ZhangYuBo HEXIN Software Co., Ltd. 
	
  历史记录 : 
================================================================*/
void CDataBufferListQueue::AddBufferObj(CDataBufferObj *pNewBuffer)
{
	ASSERT(pNewBuffer);
	// 检查一下有没有重复
#ifdef _DEBUG
	BOOL bFound = FALSE;
	POSITION pos = GetTailPosition();
	while(pos)
	{
		CDataBufferObj* pFoundObj = (CDataBufferObj*)GetPrev(pos);
		if(pFoundObj == pNewBuffer)
		{	
			bFound = TRUE;
			break;
		}
	}
	if(bFound)
	{
		ASSERT(FALSE);
		return;
	}
#endif
	AddTail(pNewBuffer);
}

/*!===============================================================
  函数功能 : 从链表中去掉一个缓冲区对象

  创建信息 : 2007年11月7日  By ZhangYuBo HEXIN Software Co., Ltd. 
	
  历史记录 : 
================================================================*/
void CDataBufferListQueue::RemoveBufferObj()
{
	CDataBufferObj* pDeleteObj = (CDataBufferObj*)RemoveHead();
	SAFE_DELETE(pDeleteObj);
}

/*!===============================================================
  函数功能 : 清空所有的缓冲区对象

  创建信息 : 2007年11月7日  By ZhangYuBo HEXIN Software Co., Ltd. 
	
  历史记录 : 
================================================================*/
void CDataBufferListQueue::EmptyAllBufferObj()
{
	POSITION pos = GetHeadPosition();
	while(pos)
	{
		CDataBufferObj* pBufferDelete = (CDataBufferObj*)GetNext(pos);
		SAFE_DELETE(pBufferDelete);
	}
	RemoveAll();
}


/*!===============================================================
  函数功能 : 将队列中的数据合并成一块完整连续的内存

  创建信息 : 2007年11月9日  By ZhangYuBo HEXIN Software Co., Ltd. 
	
  历史记录 : 
================================================================*/
void CDataBufferListQueue::MergeBuffer(CDataBufferObj &DataBufferObj)
{
	DataBufferObj.FreeBuffer();
	ULONG lTotalLength = GetTotalBufferLength();
	if(lTotalLength == 0)
	{
		return;
	}
	BYTE* pNewBuffer = DataBufferObj.Alloc(lTotalLength);
	ASSERT(pNewBuffer);
	if(pNewBuffer == NULL)
	{
		return;
	}
	CDataBufferObj* pHeadBufferObj = GetBufferObj();
	if(pHeadBufferObj)
	{
		DataBufferObj.m_ulIndexOffset = pHeadBufferObj->m_ulIndexOffset;
	}
	ULONG nTotalCopyed = 0;
	POSITION pos = GetHeadPosition();
	while(pos)
	{
		CDataBufferObj* pBufferObj = (CDataBufferObj*)GetNext(pos);
		ASSERT(pBufferObj->m_pBufferData);
		if(pBufferObj && pBufferObj->m_pBufferData)
		{
			memcpy(&(pNewBuffer[nTotalCopyed]),
					pBufferObj->m_pBufferData,
					min(pBufferObj->m_ulBufferSize,(lTotalLength - nTotalCopyed))
					);
			nTotalCopyed += pBufferObj->m_ulBufferSize;
		}
		if(nTotalCopyed >= lTotalLength)
		{
			break;
		}
	}
}

/*!===============================================================
  函数功能 : 得到全部缓冲块的总长度

  创建信息 : 2007年11月9日  By ZhangYuBo HEXIN Software Co., Ltd. 
	
  历史记录 : 
================================================================*/
ULONG CDataBufferListQueue::GetTotalBufferLength()
{
	ULONG lTotalBufferLength = 0;
	POSITION pos = GetHeadPosition();
	while(pos)
	{
		CDataBufferObj* pBufferObj = (CDataBufferObj*)GetNext(pos);
		if(pBufferObj)
		{
			lTotalBufferLength += pBufferObj->m_ulBufferSize;
		}
	}
	return lTotalBufferLength;
}
