#pragma once
#include <vector>
using namespace std;

#include "SafeLock.h"
#include "ProxyInfo.h"

class CRscProxyInfo
{
private:
	vector<ProxyInfo*>	m_VecProxyInfo;
	int					m_nIndex;

	CSafeLock*			m_pLock;

public:
	CRscProxyInfo(CString strINIPath);
	~CRscProxyInfo(void);

public:
	void GetNextSection(ProxyInfo& nextSection);

private:
	void FromINI(CString path);
	int CalcCount(CString path , vector<CString> &section);
	void Destroy();
};
