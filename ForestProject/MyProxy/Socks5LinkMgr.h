#pragma once
#include "SessionLink.h"
#include "SessionListenSocket.h"
#include "Socks5AppToSrvLink.h"
#include "Socks5ClientToAppSrvLink.h"
#include "ProxyInfo.h"

class CSocks5Mgr;
class CSocks5LinkMgr
{
private:
	CSocks5AppToSrvLink*	m_pSocks5AppToSrv;
	CSocks5ClientToAppSrvLink*	m_pSocks5ClientToAppSrv;
	CSocks5Mgr*		m_pSocks5Mgr;

	LPBYTE				m_pBuf;

public:
	LPBYTE				m_pMethodsReq;
	LPBYTE				m_pGrantReq;
	LPBYTE				m_pCMDReq;
	BOOL				m_bIsDisconnect;

	CString				m_strSrcIP;
	CString				m_strDstIP;
	BOOL				m_bIsSuccess;

public:
	CSocks5LinkMgr(void);
	~CSocks5LinkMgr(void);

public:
	void TransDataToAppSrv(LPBYTE pData, ULONG nLen);
	void TransDataToApp(LPBYTE pData, ULONG nLen);
	void CreateConnectToAppSrv();
	void AttachSocks5Mgr(CSocks5Mgr* pMgr);
	void DetachSocks5Mgr();
	void GetConfigInfo(CString& strAppSrv,UINT& nPort,UINT& nSeq);

	void OnAccept(session_event* pNotifyEvent,session_accept_data *pAcceptDetail);

 	void OnDisconnect();	//由Left与Right设置

	void SaveReqInfo(ConsulationState enumState, LPBYTE pData, ULONG nLen);
	void NotifyAppSuccess();
	void NotifyAppSrvDisconnect();		//App断开连接后通知AppSrv断也断开连接
	void NotifyAppDisconnect();

	void GetAppAddr(CString& strIP);

protected:
	void Destroy();
	void MemRecovery();	//存储转发信息的内存回收
};
