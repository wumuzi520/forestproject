#pragma once
#include "SessionLink.h"
#include "SessionListenSocket.h"

class CSocks5LinkMgr;
class CSocks5Link
{
protected:
	CSessionLink*			m_pSessionLink;
	CSocks5LinkMgr*			m_pMgr;
	LPBYTE					m_pBuf;

public:
	CSocks5Link(void);
	virtual ~CSocks5Link(void);

public:
	void AttachMgr(CSocks5LinkMgr* pMgr);
	void DetachMgr();
	void AttachMgrBuf(LPBYTE pBuf);
	void DetachMgrBuf();
	void DoDisconnect();
	virtual void SendData(LPBYTE pData,ULONG nLen);

protected:
	void Destory();
};
