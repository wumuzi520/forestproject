#pragma once
#include <vector>
using namespace std;

#include "SafeLock.h"
#include "Socks5LinkMgr.h"

class CRscLinkMgr
{
private:
	CSafeLock*					m_pLock;
	vector<CSocks5LinkMgr*>		m_VecLinkMgr;

public:
	CRscLinkMgr(void);
	~CRscLinkMgr(void);

public:
	void AddItem(CSocks5LinkMgr* pSocks5LinkMgr);
	void QueryItem();

private:
	void Destroy();
};
