#include "StdAfx.h"
#include "Socks5Mgr.h"


UINT QueryThread(LPVOID lpParam)
{
	CSocks5Mgr* pSocks5Mgr = (CSocks5Mgr*)lpParam;
	ASSERT(pSocks5Mgr);

	while(pSocks5Mgr->m_bDestroy == FALSE)
	{
		Sleep(5000);
		pSocks5Mgr->m_pRscLikMgr->QueryItem();
	}
	TRACE("!!!!!!!!!!!!!!!Thread Exit!!!!!!!!!!!!!\n");
	return 0;
}


CSocks5Mgr::CSocks5Mgr(void)
{
	m_pListenSocket = NULL;
	m_hThread = NULL;
	m_bDestroy = FALSE;

	m_pRscProxyInfo = new CRscProxyInfo("..\\ProxyInfo.ini");		//创建配置文件资源
	m_pRscLikMgr = new CRscLinkMgr;
}

CSocks5Mgr::~CSocks5Mgr(void)
{
	Destroy();
}

void CSocks5Mgr::Destroy()
{
	m_bDestroy = TRUE;
	
	WaitForSingleObject(m_hThread,INFINITE);

	SAFE_DELETE(m_pRscProxyInfo);
	SAFE_DELETE(m_pRscLikMgr);
	SAFE_DELETE(m_pListenSocket);
}


void CSocks5Mgr::StartListening(UINT nPort)
{
	//添加一个轮询线程
	DWORD dwThread;
	m_hThread = CreateThread(NULL,0,(LPTHREAD_START_ROUTINE)QueryThread,this,0,&dwThread);

	if(m_pListenSocket == NULL)
	{
		g_Log.AppendLog("LocalHost Start Listening at port %d",nPort);
		
		m_pListenSocket = new CSessionListenSocket(nPort);

		m_pListenSocket->m_AcceptEventHandler.bind(this,&CSocks5Mgr::OnAccept);
		if(!m_pListenSocket->IsListenState())
		{
			m_pListenSocket->SessionListen();
		}
	}
}

void CSocks5Mgr::OnAccept(session_event* pNotifyEvent,session_accept_data *pAcceptDetail)
{
	TRACE("CSocks5Mgr::OnAccept\n");
	if(pAcceptDetail && 
		(pAcceptDetail->m_nErrorCode == 0) && 
		pAcceptDetail->m_pListenSocket)
	{
		CSocks5LinkMgr* pMgr = new CSocks5LinkMgr;
		m_pRscLikMgr->AddItem(pMgr);
		pMgr->AttachSocks5Mgr(this);
		pMgr->OnAccept(pNotifyEvent,pAcceptDetail);
	}
}

void CSocks5Mgr::GetNextSection(ProxyInfo& nextSection)
{
	m_pRscProxyInfo->GetNextSection(nextSection);
}