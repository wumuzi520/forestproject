#ifndef __PROXY_INFO_H__
#define __PROXY_INFO_H__

#define SOCKS_VER		0x05

#define METHOD_NONE		0xff
#define METHOD_AUTH		0x02

#define SOCKS_AUTH_VER	0x01


#define REP_SUCCESS		0x00
#define CMD_CONNECT		0x01
#define FIELD_RSV		0x00

#define BUFFER_SIZE		1024


enum ConsulationState
{
	CONS_INIT,
	FIRST_REQ,
	SECOND_REQ,
	THIRD_REQ,
	FIRST_REP,
	SECOND_REP,
	CONS_DONE
};

struct ProxyInfo {
	CString	strProxyIP;
	UINT	nProxyPort;
	CString	strUserName;
	CString	strPassword;
	UINT	Seq;
	UINT	UseState;

	ProxyInfo()
	{
		strProxyIP = "";
		nProxyPort = 0;
		strUserName = "";
		strPassword = "";
		Seq = 0;
		UseState = 0;
	}
};

#endif