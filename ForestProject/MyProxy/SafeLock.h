#ifndef	__SAFELOCK__H_
#define __SAFELOCK__H_

#include <Windows.h>

class CSafeLock
{
private:
	CRITICAL_SECTION m_Lock;

public:
	CSafeLock();
	~CSafeLock();

public:
	void Lock();
	void UnLock();
};

#endif