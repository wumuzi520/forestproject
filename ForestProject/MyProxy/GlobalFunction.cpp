#include "stdafx.h"
#include "GlobalFunction.h"

void SetOptions(DWORD& dwOptions, DWORD dwValue, DWORD dwMask)
{
	dwOptions &= ~dwMask;
	dwOptions |= (dwValue & dwMask);
}

DWORD GetOptions(DWORD dwOptions, DWORD dwMask)
{
	return (dwOptions & dwMask);
}

